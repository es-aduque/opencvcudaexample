#include <iostream>

#include <opencv2/opencv.hpp>

int main() {
  std::cout << "Hello World!\n";

  std::cout << "GPU activated:" << cv::cuda::getCudaEnabledDeviceCount() << std::endl;
  if (cv::cuda::getCudaEnabledDeviceCount() == 0) {
    std::cout << "Connection to GPU failed" << std::endl;
    return 0;
  }
  cv::cuda::GpuMat image1;
}
